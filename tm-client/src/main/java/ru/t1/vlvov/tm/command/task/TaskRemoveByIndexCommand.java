package ru.t1.vlvov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.Request.TaskRemoveByIndexRequest;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Remove task by Index.";

    @NotNull
    private final String NAME = "task-remove-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(getToken());
        request.setTaskIndex(index);
        getTaskEndpoint().removeByIndex(request);
    }

}