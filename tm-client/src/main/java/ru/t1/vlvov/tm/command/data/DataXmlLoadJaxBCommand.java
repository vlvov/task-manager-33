package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.Request.DataXmlLoadJaxBRequest;

public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load data from xml file.";

    @NotNull
    private final String NAME = "data-load-xml-jaxb";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        @NotNull DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(getToken());
        getDomainEndpoint().loadDataXmlJaxB(request);
    }

}
