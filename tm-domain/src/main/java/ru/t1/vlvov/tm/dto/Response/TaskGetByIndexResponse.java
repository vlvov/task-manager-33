package ru.t1.vlvov.tm.dto.Response;

import lombok.NoArgsConstructor;
import ru.t1.vlvov.tm.model.Task;

@NoArgsConstructor
public final class TaskGetByIndexResponse extends AbstractTaskResponse {

    public TaskGetByIndexResponse(Task task) {
        super(task);
    }

}
