package ru.t1.vlvov.tm.dto.Response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListResponse extends AbstractResponse {

    @Nullable
    private List<Task> tasks;

    public TaskListResponse(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }
}
